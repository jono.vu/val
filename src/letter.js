export const letter = `# Good morning Nicole,

Happy Valentine’s Day!

Isn’t it weird that we should celebrate a day like Valentine’s day, a known for roses and jealous people complaining about being single. This guys pretty much sums it up:

![](https://paper-attachments.dropbox.com/s_21B088F8DA8D0103D199B9891D5C771E4DD32D48481CB629F54D7C3BBB637352_1581654223177_image.png)

Even we can complain that we are on opposite sides of the world, that we can’t see each other today and express love.

But we have it luckier than most to even be able to talk to each other.

So I have shared with you the way easiest for me to say I love you, via programming! As I sit here, 3:30pm my time and realising this is nowhere near enough time to deploy a full e-card v-day app, I have spent a lot of valuable time thinking today about what it means to be together and in love.

I started today quite late. Ken fell through with his plans and Tim didn’t want to go out in the rain at Traralgon or Bendigo with just me.

I decided to go check out “Pax’s”, the toy shop you mentioned in Messenger on Thursday Evening AS I WAS LEAVING Northcote!

So I headed up to get sliced beef Pho at Viet Pearl this morning - as good as ever and with much less flies than yesterday. My phone was on 7% battery and I needed to check around for toy shops that would sell this obscure Japanese bunny (I only found through way too much googling that it was Japanese and only found in very particular online stores). The Vietnamese waitress offered to charge it at the cashier but she didn’t speak much english so I had to mime the powerpoint and that my phone was out of battery. My phone was at the register so I read some of the Lord of the Rings (bootleg edition) on my kindle. It starts off very relaxing, slow-paced, but with just enough tension to hint at the rich and magnificent adventure ready to unfold. I daydreamed of what a life it must’ve been like - to live in peaceful and rolling green hills. The excitement of the little things in life going well, surrounded by family and friends. A quiet life. A life I might like to live with you one day.

Finishing the Pho, and checking my phone, to my dismay, there were no toy shops called Pax! I fervently searched for other toy shops in Melbourne and long story short, I spent hours walking around to _Keke and Kaka_, the Japanese plush toy shop I assumed you were talking about, _The Teddy Bear Shop,_ the only dedicated plush toy teddy bear shop in Melbourne, _Mooji_ in Melbourne Central and perhaps the strangest of all, an obscure and upstairs place called _One Stop Anime_. This was my last resort and going to the second level only to take a lift up, I had the impression that this place was grungy, dank and a little out of the way. A Chinese man on the lift asked me how to get to Level 4 in Chinese, before reluctantly using a mix of sign language and broken English. I tried to tell him that he could come up to Level 3 with me, then take the stairs. Level 3 was where _One Stop Anime_ was. As soon as I got off however, I realised this wasn’t the type of Japanese Toy Shop that sold plush toys. As my Chinese friend sat outside looking for the stairs to Level 4, I hesitantly approached the poster filled and cramp shop that was One Stop Anime. It seemed less like an Anime shop and more like a… how do you say it...

like this:

![](https://img.theculturetrip.com/768x432/wp-content/uploads/2017/02/12946395604_94b126f36c_b.jpg) {This image is from Japan, but One Stop Anime had much more of an “adult” vibe.}

Needless to say, I briefly spoke to the man at the counter about the blurry picture you sent me of the pink bunny with purple ribbons. He confidently said that they don’t sell it. I thought as much.

I went back down in silence with my Chinese friend, who hadn’t managed to find the stairs and instead waited at the ground floor for his meeting. I thought I’d never be able to find your bunny and I ended up heading to here, RMIT to find a way to say I love you this Valentines Day.

But I realised I didn’t need a bunny to say that (maybe I needed a website?). Scrambling around to find something that most likely didn’t exist in Melbourne Stores taught me to enjoy my time and the small things that life offers in my every day. I can’t tell you how happy I felt just knowing that I could do something for someone I loved from across the world. A tremendous sense of calm, akin to the thought that you were resting in Italy. It’s a nice feeling knowing that I could relax walking around Melbourne, without responsibilities, and you would calmly sleep and wake up to see me! I dreamt last night that you came home early and we…

hugged!

---

I’m grateful for being able to share these words with you via internet, to be able to hear about your day and see Italy with my own eyes (even if through pixelated video chat)! I’m thankful that you are enjoying your time with your family, when so many things can go wrong everything seems to be going right. Your worst worries are about buying a second \$460 coat and inauthentic Murano glass after all!! I hope you can still enjoy your time in Italy, and you had spent your time well in Venice. They say all roads lead to Rome! And I shall see you there soon!

Even though the significance of this day eludes us both, it’s nice to be able to share with you in the present moment (and you 10 hours behind) a strong connection and love that traverses continents. Two weeks of long distance is much too long without you. I cannot wait to see you when you come back and finally ask you to be my girlfriend again.

Love, Jonathan.

![](https://paper-attachments.dropbox.com/s_21B088F8DA8D0103D199B9891D5C771E4DD32D48481CB629F54D7C3BBB637352_1581655995796_image.png)
{I cover my beard in this photo for you}`
