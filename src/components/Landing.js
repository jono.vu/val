import React, { useState } from "react"

import styles from "./css/landing.module.scss"

import { letter } from "../letter.js"

import Markdown from "markdown-to-jsx"

const Landing = () => {
  const [message, setMessage] = useState("")
  const [open, setOpen] = useState(false)

  return (
    <div className={styles.container}>
      <div className={open && styles.open}>
        <div className={styles.envelope}>
          {!open && (
            <div className={styles.waxSeal} onClick={() => setOpen(true)}>
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Heart_coraz%C3%B3n.svg/1200px-Heart_coraz%C3%B3n.svg.png" />{" "}
              <h3 className={styles.message}>{message}</h3>
            </div>
          )}
        </div>
        <div className={styles.flip} />
        {open && (
          <div className={styles.letter}>
            <Markdown>{letter}</Markdown>
            <h3
              onClick={() => {
                setOpen(false)
                setMessage(`Jon and Nicole \n 14/2/2020   `)
              }}
              style={{
                color: "blue",
                textDecoration: "underline",
              }}
            >
              Close this letter here!
            </h3>
          </div>
        )}
      </div>
    </div>
  )
}

export default Landing
